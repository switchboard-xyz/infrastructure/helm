{{/*
Expand the name of the chart.
*/}}
{{- define "mychart.name" -}}
{{- trimSuffix "-" (trunc 63 (default .Chart.Name .Values.nameOverride)) }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "mychart.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- trimSuffix "-" (trunc 63 .Values.fullnameOverride) }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- trimSuffix "-" (trunc 63 .Release.Name) }}
{{- else }}
{{- trimSuffix "-" (trunc 63 (printf "%s-%s" $name .Release.Name)) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "mychart.chart" -}}
{{- trimSuffix "-" (trunc 63 (replace "+" "_" (printf "%s-%s" .Chart.Name .Chart.Version))) }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "mychart.labels" -}}
helm.sh/chart: {{ include "mychart.chart" . }}
{{ include "mychart.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ quote .Chart.AppVersion }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "mychart.selectorLabels" -}}
app.kubernetes.io/name: {{ include "mychart.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the config to use.
*/}}
{{- define "mychart.configMapName" -}}
{{- trimSuffix "-" (trunc 63 (printf "%s-%s" (include "mychart.fullname" .) "config")) }}
{{- end }}

{{/*
Create the name of the secret to use.
*/}}
{{- define "mychart.secretName" -}}
{{- trimSuffix "-" (trunc 63 (printf "%s-%s" (include "mychart.fullname" .) "secret")) }}
{{- end }}

{{/*
Create the name of the volume to use.
*/}}
{{- define "mychart.volumeName" -}}
{{- trimSuffix "-" (trunc 63 (printf "%s-%s" (include "mychart.fullname" .) "volume")) }}
{{- end }}

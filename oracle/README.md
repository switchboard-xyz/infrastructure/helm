# Switchboard Oracle

## Prerequisites
- Kubernetes 1.19+
- Helm 3+

## Clone the Git repo
```
git clone https://gitlab.com/switchboard-xyz/infrastructure/helm.git
```

Change into the chart directory inside the clone repo:

```
cd oracle
```

## Install Chart
> Note that the values provide a sample configuration created from [here](https://github.com/switchboard-xyz/examples). You'll want to replace them with your own values.

```
$ helm install [RELEASE_NAME] .
```

## Uninstall Chart
```
helm uninstall [RELEASE_NAME]
```

This removes all the Kubernetes components associated with the chart and deletes the release.

> See [helm uninstall](https://helm.sh/docs/helm/helm_uninstall) for command documentation.

## Upgrading Chart
```
$ helm upgrade [RELEASE_NAME] . --install
```

> See [helm upgrade](https://helm.sh/docs/helm/helm_upgrade) for command documentation.

## Configuration
See [Customizing the Chart Before Installing](https://helm.sh/docs/intro/using_helm#customizing-the-chart-before-installing). To see all configurable options with detailed comments, visit the chart's `values.yaml`, or run:

```
helm show values .
```
